package com.dabai.markdownq.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.dabai.dbutils.utils.DateUtils;
import com.dabai.dbutils.utils.FileUtils;
import com.dabai.markdownq.R;
import com.dabai.markdownq.bean.FileData;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Description : Other
 *
 * @author BAI
 */
public class FileAdapter extends BaseQuickAdapter<FileData, BaseViewHolder> {


    public FileAdapter(@Nullable List<FileData> data) {
        super(R.layout.item_file, data);
    }


    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, FileData fileData) {
        baseViewHolder.setText(R.id.headText,""+fileData.getFileName().charAt(0));
        baseViewHolder.setText(R.id.titleText, fileData.getFileName());
        baseViewHolder.setText(R.id.contentText, FileUtils.readText(fileData.getPath()).length()>0 ? FileUtils.readText(fileData.getPath()): "无内容");


        File file = new File(fileData.getPath());
        baseViewHolder.findView(R.id.headText).setOnClickListener(v -> {

                new MaterialAlertDialogBuilder(getContext())
                        .setTitle("文件信息")
                        .setMessage("文件名:"+fileData.getFileName()
                                +"\n字数:"+FileUtils.readText(fileData.getPath()).length()
                                +"\n文件大小:"+FileUtils.getFileSizeString(file)
                                +"\n编码方式:"+FileUtils.getFileCharset(file)
                                +"\n最后修改时间:"+DateUtils.humanInterval(new Date(file.lastModified())))
                        .setPositiveButton("确认",null)
                        .show().getWindow().setWindowAnimations(com.dabai.uitools.R.style.DBAnim_Transitions_IOS);

        });

    }




}
