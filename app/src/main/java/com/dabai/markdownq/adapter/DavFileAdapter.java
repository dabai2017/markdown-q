package com.dabai.markdownq.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.dabai.dbutils.utils.DateUtils;
import com.dabai.dbutils.utils.FileUtils;
import com.dabai.markdownq.R;
import com.paul623.wdsyncer.model.DavData;

import java.util.List;

/**
 * Description : Other
 *
 * @author BAI
 */
public class DavFileAdapter extends BaseQuickAdapter<DavData, BaseViewHolder> {


    public DavFileAdapter(@Nullable List<DavData> data) {
        super(R.layout.item_dav, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DavData davData) {
        baseViewHolder.setText(R.id.titleText,davData.getName());
        String backupTime = DateUtils.humanInterval(davData.getModified());

        baseViewHolder.setText(R.id.contentText,"备份时间:"+ (backupTime.isEmpty() ?"未知":backupTime)+" 体积:"+ FileUtils.formatFileSizeToString(davData.getContentLength()));
    }
}
