package com.dabai.markdownq.ui;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dabai.dbutils.utils.AssetsUtils;
import com.dabai.dbutils.utils.DabaiUtils;
import com.dabai.dbutils.utils.component.ActivityUtils;
import com.dabai.markdownq.MainActivity;
import com.dabai.markdownq.R;
import com.dabai.markdownq.databinding.ActivityMdHelpBinding;
import com.dabai.markdownq.utils.OtherUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;

import br.tiagohm.markdownview.css.styles.Github;
import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.core.MarkwonTheme;
import io.noties.markwon.ext.latex.JLatexMathPlugin;
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin;
import io.noties.markwon.ext.tables.TableAwareMovementMethod;
import io.noties.markwon.ext.tables.TablePlugin;
import io.noties.markwon.ext.tasklist.TaskListPlugin;
import io.noties.markwon.html.HtmlPlugin;
import io.noties.markwon.image.ImagesPlugin;
import io.noties.markwon.image.gif.GifMediaDecoder;
import io.noties.markwon.inlineparser.MarkwonInlineParserPlugin;
import io.noties.markwon.movement.MovementMethodPlugin;

public class MdHelpActivity extends AppCompatActivity {

    ActivityMdHelpBinding amhb;
    Context context;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        amhb = ActivityMdHelpBinding.inflate(getLayoutInflater());
        setContentView(amhb.getRoot());

        context = this;

        String content = AssetsUtils.loadAssetsText(context, "MarkdownHelp.md");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        if (DabaiUtils.isActivityFirstOpen(this)){
            DabaiUtils.showDialog(context,"向右滑动可以查看文档预览!\n你可以分屏显示此页面!");
        }

        View view1 = getLayoutInflater().inflate(R.layout.help_page_1, null);
        View view2 = getLayoutInflater().inflate(R.layout.help_page_2, null);

        ArrayList<View> viewList = new ArrayList<View>();// 将要分页显示的View装入数组中
        viewList.add(view1);
        viewList.add(view2);

        amhb.ViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            //下面三个回调缺一不可，否则就会编译报错
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    setTitle("Markdown语法帮助文档");
                }
                if (position == 1) {
                    setTitle("示例预览");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        PagerAdapter pagerAdapter = new PagerAdapter() {

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return viewList.size();
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                container.removeView(viewList.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(viewList.get(position));
                return viewList.get(position);
            }
        };
        amhb.ViewPager.setAdapter(pagerAdapter);

        TextView t1 = view1.findViewById(R.id.md_soc);
        TextView t2 = view2.findViewById(R.id.md_tag);

        ScrollView scr1 = view1.findViewById(R.id.scr_1);
        ScrollView scr2 = view2.findViewById(R.id.scr_2);

        scr1.setOnScrollChangeListener((view, i, i1, i2, i3) -> scr2.setScrollY(i1));
        scr2.setOnScrollChangeListener((view, i, i1, i2, i3) -> scr1.setScrollY(i1));


        t1.setText(content);

        OtherUtils.parseMd(context,t2, content + '\u3000');

    }

    @Override
    public void onMultiWindowModeChanged(boolean isInMultiWindowMode) {
        super.onMultiWindowModeChanged(isInMultiWindowMode);
        if (isInMultiWindowMode){
                ActivityUtils.startActivity(this,MainActivity.class);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}