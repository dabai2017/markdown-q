package com.dabai.markdownq.provider;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.dabai.dbutils.toast.DBToast;
import com.dabai.dbutils.utils.DabaiUtils;
import com.dabai.dbutils.utils.DateUtils;
import com.dabai.dbutils.utils.FileUtils;
import com.dabai.dbutils.utils.SharedPreferencesBasicUse;
import com.dabai.markdownq.R;
import com.dabai.markdownq.utils.OtherUtils;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Description : Other
 *
 * @author BAI
 */
public class ListWidgets extends AppWidgetProvider {
    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        DabaiUtils.showToast(context, "桌面助手,已经阵亡!");
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        DabaiUtils.showToast(context, "桌面助手,准备就绪!");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (intent.getAction().equals("666")) {
           // DabaiUtils.showToast(context, "666");
        }


    }

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        SharedPreferencesBasicUse.init(context);

        ComponentName provider = new ComponentName(context.getApplicationContext(), ListWidgets.class);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_list);
        // 获取远程View布局中的控件

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                views.setTextViewText(R.id.textView2, "总体积:" + FileUtils.getFileSizeString(context.getExternalFilesDir(null)));
                views.setTextViewText(R.id.textView3, "文档数量:" + OtherUtils.getMarkdownFilesDir(context).list().length);
                views.setTextViewText(R.id.textView4, "资源体积:" + FileUtils.formatFileSizeToString(FileUtils.getFileSize(OtherUtils.getResourcesDir(context)) + FileUtils.getFileSize(OtherUtils.getFontDir(context)) + FileUtils.getFileSize(OtherUtils.getSrcDir(context))));
                views.setTextViewText(R.id.textView5, "缓存体积:" + FileUtils.formatFileSizeToString(FileUtils.getFileSize(OtherUtils.getTmpDir(context)) + FileUtils.getFileSize(OtherUtils.getTmpHtmlDir(context))));
                views.setTextViewText(R.id.textView6, "上次打开时间:" + DateUtils.humanInterval(new Date(SharedPreferencesBasicUse.getLong("lastOpen", 0))));


                appWidgetManager.updateAppWidget(provider, views);
            }
        };
        // 开启定时任务，每5秒更新执行一次
        timer.schedule(timerTask, 5000, 5000);


        Timer timer2 = new Timer();
        TimerTask timerTask2 = new TimerTask() {
            @Override
            public void run() {

                String[] strings = new String[]{"主界面的创建按钮是可以长按的哦！"
                        , "桌面长按图标可以使用shortcut哦！"
                        , "主界面列表长按，如果有内容会显示预览哦!"
                        , "语法参考界面可以分屏哦，和主程序是分开的!"
                        , "如果没有手机屏幕锁，应用锁是不会生效的哦!"
                        , "开启水印，可以保护你的版权哦!"
                        , "无序列表按钮，长按重置计数器!"};

                views.setTextViewText(R.id.textView7, "小提示:" + strings[new Random().nextInt(strings.length)]);


                appWidgetManager.updateAppWidget(provider, views);
            }
        };
        // 开启定时任务，每5秒更新执行一次
        timer2.schedule(timerTask2, 10000, 10000);

        Intent intent = new Intent("666");
        intent.setClass(context, ListWidgets.class);
        views.setOnClickPendingIntent(R.id.lineLayout, PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));


    }

}
