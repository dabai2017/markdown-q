package com.dabai.markdownq.bean;

/**
 * Description : Other
 *
 * @author BAI
 */
public class FileData {
    String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    String path;
}
