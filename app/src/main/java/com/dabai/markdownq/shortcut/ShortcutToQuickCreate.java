package com.dabai.markdownq.shortcut;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.dabai.dbutils.toast.DBToast;
import com.dabai.dbutils.utils.DateUtils;
import com.dabai.dbutils.utils.SharedPreferencesBasicUse;
import com.dabai.dbutils.utils.SharedPreferencesUtils;
import com.dabai.markdownq.ui.EditorActivity;
import com.dabai.markdownq.utils.OtherUtils;

import java.io.File;
import java.io.IOException;

/**
 * Description : Other
 *
 * @author BAI
 */
public class ShortcutToQuickCreate extends Activity {

    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        SharedPreferencesBasicUse.init(context);
        String fileName = DateUtils.getNowTime(SharedPreferencesBasicUse.getString("rapidNamingNew", "新文件ddhhmmss"));
        File file = new File(OtherUtils.getMarkdownFilesDir(context), fileName+ ".md");

        try {
            if (file.createNewFile()) {
                DBToast.showText(context, "快速创建:"+fileName, true);
                toEditor(file.getAbsolutePath(), true);
            } else {
                DBToast.showText(context, "创建失败!", false);
            }

        } catch (IOException e) {
            DBToast.showText(context, "创建失败:" + e.getMessage(), false);
        }

        finish();

    }

    /**
     * 跳转到编辑器
     *
     * @param path 路径
     */
    public void toEditor(String path, boolean isFile) {
        Intent intent = new Intent(this, EditorActivity.class);
        intent.putExtra("path", path);
        intent.putExtra("isFile", isFile);
        intent.putExtra("isQuickCreate", false);
        startActivity(intent);
        if (SharedPreferencesUtils.getDefaultSharedPreferences(context).getBoolean("animatedTransitions", false)) {
            overridePendingTransition(com.dabai.uitools.R.anim.push_ios_in, com.dabai.uitools.R.anim.push_ios_out);
        }
    }

}
