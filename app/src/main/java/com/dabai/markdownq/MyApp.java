package com.dabai.markdownq;

import android.app.Application;

import com.tencent.bugly.Bugly;

/**
 * Description : 应用程序
 *
 * @author BAI
 */
public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //B u g l y初始化
        Bugly.init(getApplicationContext(), "daa90cc31f", false);


    }
}
