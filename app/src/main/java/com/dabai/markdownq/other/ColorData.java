package com.dabai.markdownq.other;

/**
 * Description : Other
 *
 * @author BAI
 */
public class ColorData {

     String[] COLOR_VALUE = {"#FFCDD2", "#FFE0B2", "#FFF9C4", "#C8E6C9", "#B2EBF2", "#BBDEFB", "#E1BEE7", "#FFFFFF"};

     public final static String[] writingPaperColor = {"#FCE4EC","#EDE7F6","#E0F2F1","#E8F5E9","#FFFDE7"
             ,"#FFCDD2", "#FFE0B2", "#FFF9C4", "#C8E6C9", "#BBDEFB", "#E1BEE7", "#CFD8DC", "#FFFFFF"};

}
