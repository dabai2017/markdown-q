package com.dabai.markdownq.other;

import android.text.style.ForegroundColorSpan;

public class CustomPunctuationSpan extends ForegroundColorSpan {
    public CustomPunctuationSpan() {
        super(0xFFE64A19);
    }
}