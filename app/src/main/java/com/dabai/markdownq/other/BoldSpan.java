package com.dabai.markdownq.other;

import android.graphics.Typeface;
import android.text.style.StyleSpan;

/**
 * Description : Other
 *
 * @author BAI
 */
public class BoldSpan extends StyleSpan {

    public BoldSpan() {
        super(Typeface.BOLD);
    }
}
