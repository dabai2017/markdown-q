package com.dabai.markdownq.utils;

/**
 * Description : Other
 *
 * @author BAI
 */
public class MdTextUtils {

    public static String returnInsertTableText(int column,int row) {
        int i;
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("|");
        for (i = 0; i < column; i++) {
            stringBuilder.append(" header |");
        }
        stringBuilder.append("\n|");
        for (i = 0; i < column; i++) {
            stringBuilder.append(":------:|");
        }
        stringBuilder.append("\n");
        for (int i2 = 0; i2 < row; i2++) {
            stringBuilder.append("|");
            for (i = 0; i < column; i++) {
                stringBuilder.append(" content |");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.substring(0,stringBuilder.length()-1);
    }

    public static String returnAddTableText(int column,int row) {
        int i;
        StringBuilder stringBuilder = new StringBuilder();

        for (int i2 = 0; i2 < row; i2++) {
            stringBuilder.append("|");
            for (i = 0; i < column; i++) {
                stringBuilder.append(" add |");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.substring(0,stringBuilder.length()-1);
    }

}
