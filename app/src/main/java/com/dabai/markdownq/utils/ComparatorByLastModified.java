package com.dabai.markdownq.utils;

import java.io.File;
import java.util.Comparator;

public class ComparatorByLastModified implements Comparator<File> {

    int i = 1;

    public ComparatorByLastModified() {
    }

    //传入false 为时间倒序排列
    public ComparatorByLastModified(boolean order) {
        if (!order) {
            i = -1;
        }
    }

    @Override
    public int compare(File f1, File f2) {
        long diff = f1.lastModified() - f2.lastModified();
        if (diff > 0) {
            return i;// 倒序正序控制
        } else if (diff == 0) {
            return 0;
        } else {
            return -i;// 倒序正序控制
        }
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }
}
