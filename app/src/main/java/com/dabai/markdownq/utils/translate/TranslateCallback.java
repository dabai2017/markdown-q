package com.dabai.markdownq.utils.translate;

/**
 * 翻译回调
 */

public interface TranslateCallback{
    public void onTranslateDone(String result);
}